import 'package:flutter/material.dart';
import 'package:fuel_smart/dashboard.dart';
import 'package:fuel_smart/user.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
    providers: [ChangeNotifierProvider(create: (_) => User())],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final txtUserNameController = TextEditingController();
  final txtUserPwdController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.blueGrey,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Provider Login"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Center(
            child: Column(
              children: [
                TextField(
                  controller: txtUserNameController,
                  decoration: const InputDecoration(hintText: "Enter username"),
                ),
                TextField(
                  controller: txtUserPwdController,
                  decoration: const InputDecoration(hintText: "Enter password"),
                ),
                TextButton(
                    onPressed: () {
                      Provider.of<User>(context, listen: false).signIn(
                          txtUserNameController.text.toString(),
                          txtUserPwdController.text.toString());
                      Get.to(() => Dashboard());
                    },
                    child: const Text("Login"))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
